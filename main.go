package main

import (
	"crypto/x509"
	"crypto/tls"
	"errors"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strings"

	goopt "github.com/droundy/goopt"
)

func main() {
	err := Run(os.Args[1:])
	if err != nil {
		log.Fatal("[ERROR] ", err)
	}
}

var secure = false

var ca = goopt.String([]string{"--ca"}, "", "Trusted Certificate Authority file path")
var capath = goopt.String([]string{"--capath"}, "", "Path to a directory containing multiple CA files")

func Run(args []string) error {
	goopt.Description = func() string {
		return "PAM auth/account module for checking authn and authz using HTTPS Basic auth."
	}
	goopt.Version = "1.0"
	goopt.Summary = "echo password | PAM_TYPE=auth PAM_USER=username pam-https https://your-page/authn-endpoint --ca[path] /path/to/ca\n" +
	"PAM_TYPE=account PAM_USER=username pam-https https://your-page/authz-endpoint --ca[path] /path/to/ca"
	goopt.Parse(nil)

	if len(goopt.Args) != 1 {
		return errors.New("Invalid arguments.")
	}

	url := goopt.Args[0]

	tr := &http.Transport{}
	if secure {
		if !strings.HasPrefix(url, "https://") {
			return errors.New("URL does not start with https://, refusing to continue.")
		}

		certs := x509.NewCertPool()
		if *ca != "" {
			if err := AddCAFile(certs, *ca); err != nil {
				return err
			}
		} else if *capath != "" {
			if err := AddCAPath(certs, *capath); err != nil {
				return err
			}
		} else {
			return errors.New("No --ca/--capath set, there is no way to verify the endpoint authenticity, refusing to continue.")
		}

		tr = &http.Transport{
			TLSClientConfig: &tls.Config{
				RootCAs: certs,
				MinVersion: tls.VersionTLS12,
			},
			DisableCompression: true,
		}
	}

	client := &http.Client{Transport: tr}

	username, err := GetUsername()
	if err != nil {
		return err
	}

	password, err := GetPassword()
	if err != nil {
		return err
	}

	if err := CheckAuth(client, url, username, password); err != nil {
		return err
	}

	log.Printf("[INFO] %s authenticated!", username)
	return nil
}

func GetUsername() (string, error) {
	username := os.Getenv("PAM_USER")
	if username == "" {
		return "", errors.New("No PAM_USER provided.")
	}
	return username, nil
}

func GetPassword() (string, error) {
	pam_type := os.Getenv("PAM_TYPE")
	if pam_type == "auth" {
		return ReadPassword()
	} else if pam_type == "account" {
		return "", nil
	} else {
		return "", errors.New("No PAM_TYPE provided.")
	}
}

func ReadPassword() (string, error) {
	bytes, err := ioutil.ReadAll(os.Stdin)
	if err != nil {
		return "", err
	}

	return strings.TrimSuffix(strings.TrimSuffix(string(bytes), "\x00"), "\n"), nil
}

func CheckAuth(client *http.Client, url, username, password string) error {
	r, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return err
	}
	r.SetBasicAuth(username, password)
	resp, err := client.Do(r)
	if err != nil {
		return err
	}
	resp.Body.Close()

	if resp.StatusCode < 200 || resp.StatusCode > 299 {
		return errors.New(resp.Status)
	}

	return nil
}
