package main

import (
	"crypto/x509"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
	"fmt"
)

func AddCAFile(pool *x509.CertPool, file string) error {
	pem, err := ioutil.ReadFile(file)
	if err != nil {
		return fmt.Errorf("Error loading CA File: %s", err)
	}

	if ok := pool.AppendCertsFromPEM(pem); !ok {
		return fmt.Errorf("Error loading CA File: Couldn't parse PEM in: %s", file)
	}

	return nil
}

func AddCAPath(pool *x509.CertPool, path string) error {
	err := filepath.Walk(path, func(file string, info os.FileInfo, err error) error {
		if err != nil || info.IsDir() {
			return nil
		}

		if err := AddCAFile(pool, file); err != nil {
			log.Print(err)
		}

		return nil
	})
	if err != nil {
		return err
	}

	return nil
}
