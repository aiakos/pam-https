# pam-https - PAM auth/account module using HTTPS Basic auth.

pam-https is a PAM module that can be used to authenticate and authorize users with an external service(s) exposing HTTPS Basic authentication.

## Authentication - PAM "auth" module type
```
auth requisite pam_exec.so quiet expose_authtok /usr/local/bin/pam-https https://authn/endpoint/ --ca /path/to/trusted/ca.crt
```

Authentication is the process of verifying the identity of an user. pam-https takes the user's login and password, and forwards them to the configured HTTPS endpoint - and if it responds with a 2xx status code, assumes that the credentials are correct.

## Authorization - PAM "account" module type
```
account required pam_exec.so quiet /usr/local/bin/pam-https https://authz/endpoint/ --ca /path/to/trusted/ca.crt
```

Authorization is the process of verifying permissions to do something. pam-https takes the user's login and an empty password and forwards them to the configured HTTPS endpoint - and if it responds with a 2xx status code, assumes that the user should be allowed to log in.

## HTTPS certificates
pam-https doesn't use system SSL certificates by default, as:
1. It takes one CA to get compromised to issue a valid certificate for your domain, and compromise your system.
2. Golang currently does not support requiring and verifying OCSP staples, so there is no good way to revoke a certificate.

For the best security, we recommend using short-lived (in the order of hours) CA and short-lived HTTPS certificates, and rotating the CA certificate automatically.

If you want your system to be INSECURE, you can use --ca /etc/ssl/certs/ca-certificates.crt to use the system certificate pool.
